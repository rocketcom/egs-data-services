# EGS Data Services README
The Astro Sample Apps Suite relies on a number of web services to provide data for things like satellite passes. It is comprised of:

* A mongo database
* A node express-based web service application

## Prerequisites
1. A UNIX-like OS (Linux, Mac OS, FreeBSD, etc.)
2. [MongoDB](https://www.mongodb.com/)
3. [Node.js](https://nodejs.org/)
4. [npm](https://www.npmjs.com/get-npm)
5. [forever](https://www.npmjs.com/package/forever) node package (optional) 

## Installing the database
The "data" directory contains a folder named "egs_db_dump." To load the data into Mongo:

1. `cd data`
2. `mongorestore egs_db_dump`

This will create an populate a database called "egs."

## Changing Configuration Options
Two values are set in config/config.json:

* Port on which the Web Services run
* Name of the Mongo database backing the services

Config files for local, development, and production environments are included in the config directory. You may also create your own following the same format. In order for the service to work, you must symlink either one of the existing files or your own to config.json. For example:

    ln -s config.dev.json config.json

## Starting the Web Service
### In development
    node index.js

### In production
    forever start index.js

### Use with a Web Server
In some cases, it may be more convenient to use the Web Services behind a web server like Nginx by proxy-passing. Describing the configuration of a web server is beyond the scope of this document, but, a sample Nginx config file, "sample-nginx-config" is provided in the "config" directory for reference.

## Service End Points

### Watchers
The TT&C Application Suite includes an unimplemented feature called "Watchers" that will allow users to add or remove items from a list of components to appear in Watcher tables and graphs that appear in the Monitor and Command apps. (Currently, the list cannot be modified).

#### `/watchers`
A GET method that returns all available components and their watch state.

#### `/watchers/active`
A GET method that returns only actively watched items. This is used to populate the Watcher tables.

#### `/watchers/add/:id`
A PUT method that adds an item to the active Watcher list. (Currently unimplented in web application).

#### `/watchers/remove/:id`
A PUT method that removes an item to the active Watcher list. (Currently unimplented in web application).

### Equipment Status

#### `/equipment-status/hourly`
A GET method that returns data to populate the Trending Equipment Status graph in the GRM Dashboard app.

### Contacts
"Contacts" also known as "Passes" represent periods of contact between a satellite and a ground station. The EGS database contains a year's worth of simulated contacts for a satellite constellation based on the GPS system.

#### `/contacts/recent`
A GET method that returns a day's worth of contacts for the constellation.

#### `/contacts/start-aos/:startAos/end-aos/:endAos`
A GET method that returns all of the contacts that start between two UNIX timestamps (:startAos and :endAos).

#### `/contacts/satellite/:satellite/start-aos/:startAos/end-aos/:endAos`
A GET method that returns all of the contacts that start between two UNIX timestamps (:startAos and :endAos) for a specific satellite.

## Support
We welcome your questions and comments! Please contact us at <UXSupport@rocketcom.com> and we'll get back to you shortly (generally within one business day).