'use strict';
const config = require('./config/config.json');
const ObjectId = require('mongodb').ObjectID;
const Chance = require('chance');
const chance = new Chance();
const express = require('express');
const app = express();
const port = config.port;
const db_name = config.db_name;
const MongoClient = require('mongodb').MongoClient;
const mongoUrl = 'mongodb://localhost:27017/';
const bodyParser = require('body-parser');
const ONE_SECOND = 1000;
const ONE_MINUTE = 60;
const ONE_HOUR = 60 * ONE_MINUTE;
const ONE_DAY = 24 * ONE_HOUR;
const MEASURAND_TYPES = ["rf","digital","comms","facilities"];

let formatTimestamp = (timestamp) => {
  let year = timestamp.getUTCFullYear();
  let month = timestamp.getUTCMonth() + 1; // Javascript months are a zero-indexed array
  if ( month < 10 ) {
    month = "0" + month;
  }
  let date = timestamp.getUTCDate();
  if ( date < 10 ) {
    date = "0" + date;
  }
  let hours = timestamp.getUTCHours();
  if ( hours < 10 ) {
    hours = "0" + hours;
  }
  let minutes = "00";
  let seconds = "00";
  let timestampString = year + "-" + month + "-" + date + "T" + hours + ":" + minutes + ":" + seconds + "Z";
  return timestampString;
}

// Watcher Methods
app.get('/watchers', function(req, res) {
  MongoClient.connect(mongoUrl, { useNewUrlParser: true }, function(err, db) {
    if (err) {
      throw err;
    }
    let dbo = db.db(db_name);
    dbo.collection("watchers").find().toArray(function(err, result) {
      if (err) {
        throw err;
      }
      res.send(result);
      db.close();
    });

  });
});

app.get('/watchers/active', function(req, res) {
  MongoClient.connect(mongoUrl, { useNewUrlParser: true }, function(err, db) {
    if (err) {
      res.sendStatus(500); // there was an error;
    }
    let dbo = db.db(db_name);
    dbo.collection("watchers").find({watcher: 1}).toArray(function(err, result) {
      if (err) {
        res.sendStatus(500); // there was an error;
      }
      res.send(result);
      db.close();
    });

  });
});

app.put('/watchers/add/:id', function(req, res) {
  let watcherId = req.params.id;
  if ( !ObjectId.isValid(watcherId) ) {
    res.sendStatus(404);
  }

  MongoClient.connect(mongoUrl, { useNewUrlParser: true }, function(err, db) {
    if (err) {
      res.sendStatus(500); // there was an error;
    }
    let dbo = db.db(db_name);
    dbo.collection("watchers").findOne({_id: ObjectId(watcherId)}).then(function(result) {
      if ( result === null ) {
        res.sendStatus(404)
      } else if ( result.watcher ) {
        res.sendStatus(304); // Item was already on the watch list, send an 'Unchanged' status
      } else {
        dbo.collection("watchers").updateOne(
          {_id: ObjectId(watcherId)},
          {$set: {watcher: 1}}
        ).then(function(result) {
          res.sendStatus(200);
        })
      }
      db.close();
    });

  });
});

app.put('/watchers/remove/:id', function(req, res) {
  let watcherId = req.params.id;
  if ( !ObjectId.isValid(watcherId) ) {
    res.sendStatus(404);
  }

  MongoClient.connect(mongoUrl, { useNewUrlParser: true }, function(err, db) {
    if (err) {
      res.sendStatus(500); // there was an error;
    }
    let dbo = db.db(db_name);
    dbo.collection("watchers").findOne({_id: ObjectId(watcherId)}).then(function(result) {
      if ( result === null ) {
        res.sendStatus(404);
      } else if ( !result.watcher ) {
        res.sendStatus(304); // Item was already not on the watch list, send an 'Unchanged' status
      } else {
        dbo.collection("watchers").updateOne(
          {_id: ObjectId(watcherId)},
          {$set: {watcher: 0}}
        ).then(function(result) {
          res.sendStatus(200);
        })
      }
      db.close();
    });

  });
});
// Equipment Methods
// Generate all hourly records
app.get('/equipment-status/hourly', function(req, res) {
  let d = new Date();
  let todayStart = new Date(d.getUTCFullYear(),d.getUTCMonth(),d.getUTCDate(),0,0,0);
  let todayStartTimestamp = todayStart.getTime();
  let todayEnd = new Date(d.getUTCFullYear(),d.getUTCMonth(),d.getUTCDate(),23,59,59);
  let todayEndTimestamp = todayEnd.getTime();
  let startTimestamp = new Date(todayStartTimestamp - (3 * ONE_DAY));
  let startUTC = startTimestamp.getTime();
  let endTimestamp = new Date(todayEndTimestamp + (3 * ONE_DAY));
  let endUTC = endTimestamp.getTime();
  let records = [];
  let record = {};

  for ( let t = startUTC; t < endUTC; t = t + ONE_HOUR ) {
    let ts = new Date(t);
    let measurands = [];
    let measurand = {};
    record._id= chance.guid();
    record.dateTimeUTC = formatTimestamp(ts);
    for ( let m = 0; m < MEASURAND_TYPES.length; m++ ) {
      measurand.category = MEASURAND_TYPES[m];
      measurand.busy = chance.natural({min: 65, max: 78});
      measurand.busyAbove = chance.natural({min: 4, max: 8});
      measurand.idle = chance.natural({min: 4, max: 14});
      measurand.down = 100 - (measurand.busy + measurand.busyAbove + measurand.idle);
      measurands.push(measurand);
      measurand = {};
    }
    record.measurands = measurands;
    records.push(record);
    record = {};
  }
  res.send(records);
});


// Start APIs for contacts
app.get('/contacts/recent', function(req, res) {
  const n = new Date();
  const c = new Date();
  const now = Math.floor(c.getTime()/ONE_SECOND)
  n.setUTCHours(0,0,0);
  const startAos = Math.floor(n.getTime()/ONE_SECOND);
  n.setUTCHours(23,10,0);
  const endAos = Math.floor(n.getTime()/ONE_SECOND);
  MongoClient.connect(mongoUrl, { useNewUrlParser: true }, function(err, db) {
    if (err) {
      res.sendStatus(500); // there was an error;
    }
    let dbo = db.db(db_name);
    dbo.collection("contacts").find({ contactBeginTimestamp: {$gte: startAos,  $lte: endAos } }).sort({contactBeginTimestamp: 1}).toArray(function(err, results) {
      if (err) {
        throw err;
      }
      let formattedResults = [];
      for ( let c = 0; c < results.length; c++ ) {
        let result = results[c];
        if ( now > result.contactEndTimestamp) {
          let didFail = chance.bool({likelihood: 10});
          if ( didFail ) {
            result.contactResolution = 'failed';
            result.contactResolutionStatus = 'critical';
          } else {
            result.contactResolution = 'complete';
            result.contactResolutionStatus = 'normal';
          }
        } else if ( now > result.contactBeginTimestamp && now < result.contactEndTimestamp ) {
          result.contactResolution = 'pass';
          result.contactResolutionStatus = 'standby'
        } else if ( result.contactBeginTimestamp - (15 * ONE_MINUTE) < now ) {
          result.contactResolution = 'prepass';
          result.contactResolutionStatus = 'standby';
        } else {
          result.contactResolution = 'scheduled';
          result.contactResolutionStatus = 'off';
        }
        formattedResults.push(result);
      }
      res.send(formattedResults);
      db.close();
    });

  });
});

// 1553275860
// 1553275680

app.get('/contacts/start-aos/:startAos/end-aos/:endAos', function(req, res) {
  var startAos = parseInt(req.params.startAos);
  var endAos = parseInt(req.params.endAos);

  MongoClient.connect(mongoUrl, { useNewUrlParser: true }, function(err, db) {
    if (err) {
      res.sendStatus(500); // there was an error;
    }
    let dbo = db.db(db_name);
    dbo.collection("contacts").find({ contactBeginTimestamp: {$gte: startAos,  $lte: endAos } }).sort({contactBeginTimeStamp: 1}).toArray(function(err, result) {
      if (err) {
        throw err;
      }
      res.send(result);
      db.close();
    });

  });
});

app.get('/contacts/satellite/:satellite/start-aos/:startAos/end-aos/:endAos', function(req, res) {
  var startAos = parseInt(req.params.startAos);
  var endAos = parseInt(req.params.endAos);
  var satellite = req.params.satellite.toUpperCase();
  //  res.send(startAos + '|' + endAos);
  MongoClient.connect(mongoUrl, { useNewUrlParser: true }, function(err, db) {
    if (err) {
      res.sendStatus(500); // there was an error;
    }
    let dbo = db.db(db_name);
    dbo.collection("contacts").find({ $and: [{ contactBeginTimestamp: { $gte: startAos } }, { contactEndTimestamp: { $lte: endAos } }, { satellite: satellite }] }).toArray(function(err, result) {
      if (err) {
        res.sendStatus(500); // there was an error;
      }
      res.send(result);
      db.close();
    });

  });
});

app.listen(port, () => {
  console.log('EGS Web Service started on port %i', port);
});
